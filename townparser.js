import { BasicTerritoryParser } from "./parser.js";
import { biomeParser } from "./biomeparser.js"

class PingData {
    constructor (name, alliance, capital, members, territory, continent, strength, territories, type, changed_data) {
        this.name = name;
        this.alliance = alliance;
        this.capital = capital;
        this.members = members;
        this.territory = territory;
        this.continent = continent;
        this.strength = strength + " Strength";
        this.territories = territories;
        this.type = type;
        this.changed_data = changed_data;
    }
}

function PingDataCreator(input, type, changed_data) {
    var output = new PingData (input.name, input.alliance, input.capital, input.members, input.territory, input.continent, input.strength, input.territories, type, changed_data);
    biomeParser(input.territory)
    return output;
}

export function townParser (current_town_dictionary, prev_town_dictionary, memberPing) {
    var pings = [];

    for (const hash in current_town_dictionary) {
		if (!(hash in prev_town_dictionary)) {
			const created_town = PingDataCreator(current_town_dictionary[hash], "town_create");
            created_town.territory = BasicTerritoryParser(created_town.territory)
            //console.log("Created Town: " + created_town)
			pings.push(created_town);
		}
        if (!(hash in current_town_dictionary)) {
			const deleted_town = PingDataCreator(prev_town_dictionary[hash], "town_delete");
			//Do stuff here!
			delete prev_town_dictionary[hash];
            //console.log("Deleted Town: " + deleted_town)
		} 
        if (hash in prev_town_dictionary && prev_town_dictionary[hash].name !== current_town_dictionary[hash].name) {
            const renamed_town = current_town_dictionary[hash];
            //Do stuff here!
            //console.log("Renamed Town: " + renamed_town)
        } 
        if (hash in prev_town_dictionary && prev_town_dictionary[hash].alliance !== current_town_dictionary[hash].alliance) {
            const realigned_town = current_town_dictionary[hash];
            //Do stuff here!
            //console.log("Realigned Town: " + realigned_town)
        } 
        if (hash in prev_town_dictionary && prev_town_dictionary[hash].capital !== current_town_dictionary[hash].capital) {
            const new_capital_town = current_town_dictionary[hash];
            //Do stuff here!
            //console.log("New Capital Town: " + new_capital_town)
        } 
        if (hash in prev_town_dictionary && prev_town_dictionary[hash].strength !== current_town_dictionary[hash].strength) {
            const strength_changed_town = current_town_dictionary[hash];
            //Do stuff here!
            //console.log("Town Strength Change: " + strength_changed_town)
        } 
	}

    if (memberPing == true) {
        for (const hash in current_town_dictionary) {
            if (hash in prev_town_dictionary && prev_town_dictionary[hash].members !== current_town_dictionary[hash].members) {
                const member_changed_town = current_town_dictionary[hash];
                //Do stuff here!
                //console.log("Town Member Change: " + member_changed_town)
                } 
        }        
    }

    var data = {
        pings: pings,
        current_town_dictionary: current_town_dictionary,
        prev_town_dictionary: prev_town_dictionary,
    }
    return data;
}