import * as images from "./images.js";

import lib_fs from "fs"
//const images = JSON.parse(lib_fs.readFileSync("static_config.json")).icons.Biomes

export function biomeParser (input) {
    if (input.includes("Forest") == true) {
        input = ":deciduous_tree: " + input
    } else if(input.includes("Birch Forest")) {
        input = ":deciduous_tree: " + input
    } else if(input.includes("Clay Cliffs")) {
        input = ":person_climbing: " + input
    } else if(input.includes("Sunset Isles")) {
        input = ":island: " + input
    } else if(input.includes("Mushroom Isle")) {
        input = ":mushroom: " + input
    } else if(input.includes("Plains")) {
        input = ":corn: " + input
    } else if(input.includes("Marsh")) {
        input = ":ear_of_rice: " + input
    } else if(input.includes("Mountains")) {
        input = ":mountain: " + input
    } else if(input.includes("Ancient Forest")) {
        input = ":bamboo: " + input
    } else if (input.includes("Jungle")) {
        input = ":hibiscus: " + input
    } else if(input.includes("Ancient Jungle")) {
        input = ":hibiscus: " + input
    } else if(input.includes("Swamp")) {
        input = ":ear_of_rice: " + input
    } else if(input.includes("Desert")) {
        input = ":cactus: " + input
    } else if(input.includes("Mesa")) {
        input = ":desert: " + input
    } else if(input.includes("Dead Lands")) {
        input = ":skull_crossbones: " + input
    } else if(input.includes("Savannah")) {
        input = ":palm_tree: " + input
    } else if(input.includes("Moor")) {
        input = ":ear_of_rice: " + input
    } else if(input.includes("Roofed Forest")) {
        input = ":deciduous_tree: " + input
    } else if(input.includes("Taiga")) {
        input = ":evergreen_tree: " + input
    } else if(input.includes("Mega Taiga")) {
        input = ":evergreen_tree: " + input
    } else if(input.includes("Ice Wastes")) {
        input = ":ice_cube: " + input
    } else if(input.includes("Ice Mountains")) {
        input = ":mountain_snow: " + input
    } else if(input.includes("Balak")) {
        input = ":volcano: " + input
    } else if(input.includes("Arvan Bluffs")) {
        input = ":hibiscus: " + input
    } else if(input.includes("Jade Highlands")) {
        input = ":sunrise_over_mountains: " + input
    } else if(input.includes("Verdant Hollows")) {
        input = ":hibiscus: " + input
    } else if(input.includes("Rivi Shores")) {
        input = ":island: " + input
    } else if(input.includes("Stony Atoll")) {
        input = ":moai: " + input
    } else if(input.includes("Cherry Grove")) {
		input = ":cherry_blossom: " + input
	}
return input;
}

export function BiomeImageParser (input) {
    input = input.replaceAll(/ /gm,"").replaceAll(/\d/gm, "")
    var output = images[input];
    return output;
}

export function ContinentParser (input) {
    var output;
    switch (input) {
        case "Ascalon":
            output = ":deciduous_tree: " + input;
        break;
        case "Garama":
            output = ":hibiscus: " + input;
        break;
        case "Kalros":
            output = ":ice: " + input;
        break;
        case "Balak":
            output = ":volcano: " + input;
        break;
        case "Rivina":
            output = ":palm_tree: " + input;
        break;
    }
    return output;
}