import fetch from "node-fetch";
import * as lib_fetch from "node-fetch";
import { stringify } from "querystring";
import { json } from "stream/consumers";
import { timeStamp } from "console";
import { calculate_fight_hash } from "./main.js";
import lib_fs from "fs";
import { status } from "./status.js";
import { descriptionMS, bountifulEmojiParser, townAllianceStrengthParser, individualTownAllianceParser, emojiBoldCleanup } from "./parser.js";
import { biomeParser, BiomeImageParser, ContinentParser } from "./biomeparser.js"
import * as images from "./images.js";
import { resourceLimits } from "worker_threads";
import { get } from "http";

const icons = JSON.parse(lib_fs.readFileSync("static_config.json")).icons
const mention = JSON.parse(lib_fs.readFileSync("dynamic_config.json")).ping
const environment = JSON.parse(lib_fs.readFileSync("dynamic_config.json")).environment
const channels = JSON.parse(lib_fs.readFileSync("static_config.json")).channels
const keys = JSON.parse(lib_fs.readFileSync("static_config.json")).webhook_keys
const colors = JSON.parse(lib_fs.readFileSync("static_config.json")).colors
const rules = JSON.parse(lib_fs.readFileSync("dynamic_config.json")).rules
const servers = JSON.parse(lib_fs.readFileSync("static_config.json")).servers

var current_server_id;
var current_server_invite;

switch (environment) {
    case "DEVELOPER ENVIRONMENT":
        current_server_id = servers.FoxBox.server_id
        current_server_invite = servers.FoxBox.server_invite
    break;
    case "RELEASE ENVIRONMENT":
        current_server_id = servers.Conquest_Watcher.server_id
        current_server_invite = servers.Conquest_Watcher.server_invite
    break;
}

export function sleep7573_lmao(...args) {
	return new Promise(resolve => setTimeout(resolve, ...args));
}

export async function FightSend (fightData) {
    console.log(fightData)
    var continentKey = "";
    for (let x = 0; x < fightData.length; x++) {
        if (environment == "DEVELOPER ENVIRONMENT") {
            continentKey = channels.Developer
        }
        var currentFightData = fightData[x];
        var contentMessage;

        var embedData = {
            title: "",
            type: "rich",
            description: "**:hourglass: Fight Time:** :watch: <t:" + currentFightData.starttime + ">",
            timestamp: new Date().toISOString(),
            color: parseInt(colors[fightData[x].continent]),
            footer: {
                text: "🔎 Conquest Watcher by FoxyBearGames",
                icon_url: icons.Foxy,
            },
            author: {
                name:"Conquest Watcher",
                url: current_server_invite,
                icon_url: icons.ConquestWatcher,
            }
        };

        continentKey = keys[fightData[x].continent]

        console.log(continentKey)

        embedData.title = townAllianceStrengthParser(currentFightData.defender, currentFightData.defenderalliance, currentFightData.defenderstrength) + " is Under Attack by " + townAllianceStrengthParser(currentFightData.attacker, currentFightData.attackeralliance, currentFightData.attackerstrength) + " on **" + biomeParser(currentFightData.territory) + "**"
        contentMessage = ":crossed_swords: " + "**Battle** for **" + biomeParser(currentFightData.territory) + "** :crossed_swords:"

        // Description Extensions
        try {var descriptionExtensionTerritory = "\n**:european_castle: Territory:** :medal: " + currentFightData.territory} catch {}
        try {var descriptionExtensionContinent = "\n**:earth_americas: Continent:** " + ContinentParser(currentFightData.continent)} catch {}
        try {var descriptionExtensionBuff = "\n**:trophy: Victory:** :classical_building: " + currentFightData.buff} catch {}
        try {var descriptionExtensionAttackerVictory = "\n**:dagger: Attacker Victory:** :star: " + currentFightData.strength.attackerWin} catch {}
        try {var descriptionExtensionDefenderVictory = "\n**:shield: Defender Victory:** :star: " + currentFightData.strength.defenderWin} catch {}
        try {var descriptionExtensionConquestPoints = "\n**:trophy: Attacker Victory:** :coin: " + currentFightData.defenderstrength + " Conquest Points"} catch {}
        try {var descriptionExtensionCPVictory = "\n**:trophy: Victory:** :coin: " + currentFightData.defenderstrength + " Conquest Point Territory"} catch {}
        try {var descriptionExtensionCPLoss = "\n**:trophy: Loss:** :coin: " + currentFightData.attackerstrength + " Conquest Point Territory"} catch {}
        try {var descriptionExtensionCapBounty = "\n**:moneybag: Capital Bounty:** " + bountifulEmojiParser(currentFightData.bounty)} catch {}
        try {var descriptionExtensionBountyResources = "\n**:factory: Industry Resource Bounty:** :oil: " + currentFightData.resources.replaceAll(/Industry Resources/gm, "Industry Resource Bounty")} catch {}
        try {var descriptionExtensionBountyExpire = "\n**:hourglass: Expiration:** :watch: <t:" + currentFightData.expire + ">"} catch {}

        if (descriptionExtensionAttackerVictory == "\n**:dagger: Attacker Victory:** :star: 0 Strength") {
            descriptionExtensionAttackerVictory = ""
        }
        if (descriptionExtensionDefenderVictory == "\n**:shield: Defender Victory:** :star: 0 Strength") {
            descriptionExtensionDefenderVictory = ""
        }
        
        console.log(currentFightData.buff)

        switch (currentFightData.type) {
            case "continent-attack":
                contentMessage = mention + contentMessage
                embedData.description = embedData.description + descriptionExtensionTerritory + descriptionExtensionContinent + descriptionExtensionAttackerVictory + descriptionExtensionDefenderVictory
            break;
            case "continent-neutral":
                contentMessage = contentMessage
                embedData.title = "**" + biomeParser(currentFightData.territory) + "** is Under Attack by " + townAllianceStrengthParser(currentFightData.attacker, currentFightData.attackeralliance, currentFightData.attackerstrength)
                embedData.description = embedData.description + descriptionExtensionTerritory + descriptionExtensionContinent
            break;
            case "balak-neutral-buff":
                contentMessage = ":crossed_swords: " + "**Battle for " + biomeParser(currentFightData.territory) + "** (:classical_building: **" + currentFightData.buff + "**) :crossed_swords:"
                embedData.title = "**" + biomeParser(currentFightData.territory) + "** is Under Attack by " + townAllianceStrengthParser(currentFightData.attacker, currentFightData.attackeralliance, currentFightData.attackerstrength)
                embedData.description = embedData.description + descriptionExtensionTerritory + descriptionExtensionContinent + descriptionExtensionBuff /*+ descriptionExtensionAttackerVictory*/
            break;
            case "balak-neutral":
                contentMessage = contentMessage
                embedData.title = "**" + biomeParser(currentFightData.territory) + "** is Under Attack by " + townAllianceStrengthParser(currentFightData.attacker, currentFightData.attackeralliance, currentFightData.attackerstrength)
                embedData.description = embedData.description + descriptionExtensionTerritory + descriptionExtensionContinent
            break;
            case "balak-attack":
                contentMessage = mention + contentMessage
                embedData.description = embedData.description + descriptionExtensionTerritory + descriptionExtensionContinent /*+ descriptionExtensionAttackerVictory + descriptionExtensionDefenderVictory*/
            break;
            case "balak-attack-buff":
                contentMessage = mention + ":crossed_swords: " + "**Battle for " + biomeParser(currentFightData.territory) + "** (:classical_building: **" + currentFightData.buff + "**) :crossed_swords:"
                embedData.description = embedData.description + descriptionExtensionTerritory + descriptionExtensionContinent + descriptionExtensionBuff /*+ descriptionExtensionAttackerVictory + descriptionExtensionDefenderVictory*/
            break;
            case "rivina-neutral":
                contentMessage = ":crossed_swords: " + "**Battle** for **" + biomeParser(currentFightData.territory) + "** (:coin: **" + currentFightData.defenderstrength + " Conquest Points**)"  + " :crossed_swords:"
                embedData.title = "**" + biomeParser(currentFightData.territory) + "** is Under Attack by " + townAllianceStrengthParser(currentFightData.attacker, currentFightData.attackeralliance, currentFightData.attackerstrength)
                embedData.description = embedData.description + descriptionExtensionTerritory + descriptionExtensionContinent + descriptionExtensionConquestPoints
            break; 
            case "rivina-attack":
                contentMessage = mention + ":crossed_swords: " + "**Battle** for **" + biomeParser(currentFightData.territory) + "** (:coin: **" + currentFightData.defenderstrength + " Conquest Points**) :crossed_swords:"
                embedData.description = embedData.description + descriptionExtensionCPVictory + descriptionExtensionCPLoss
            break;
            case "capital-bountiful":
                contentMessage = mention + ":triangular_flag_on_post: " + "**Capital Bountiful** on **" + biomeParser(currentFightData.territory) + "** :triangular_flag_on_post:"
                embedData.title = "**:triangular_flag_on_post: Capital Bountiful** Against " + individualTownAllianceParser(currentFightData.defender, currentFightData.defenderalliance) + " on **" + biomeParser(currentFightData.territory) + "**"
                embedData.description = descriptionExtensionBountyExpire + descriptionExtensionTerritory + descriptionExtensionContinent + descriptionExtensionCapBounty + descriptionExtensionBountyResources
                continentKey = keys.Reinforcements
            break;
            case "capital-bountiful-attack":
                contentMessage = mention + ":loudspeaker: " + "**Capital Bountiful Raid** for **" + biomeParser(currentFightData.territory) + "** :loudspeaker:"
                embedData.title = individualTownAllianceParser(currentFightData.defender, currentFightData.defenderalliance) + " is Under Attack by " + individualTownAllianceParser(currentFightData.attacker, currentFightData.attackeralliance) + " on **" + biomeParser(currentFightData.territory) + "**"
                embedData.description = embedData.description + descriptionExtensionTerritory + descriptionExtensionContinent + descriptionExtensionCapBounty + descriptionExtensionBountyResources
                continentKey = keys.Reinforcements
            break;
            case "continent-reinforcements":
                contentMessage = mention + ":mega: " + "**Reinforced Battle** for **" + biomeParser(currentFightData.territory) + "** :mega:"
                embedData.description = embedData.description + descriptionExtensionTerritory + descriptionExtensionContinent + descriptionExtensionAttackerVictory + descriptionExtensionDefenderVictory                
                continentKey = keys.Reinforcements
            break;
        }

        const event = { 
            name: emojiBoldCleanup(contentMessage),
            privacy_level: 2,
            scheduled_start_time: currentFightData.starttimeISO,
            scheduled_end_time: currentFightData.endtimeISO,
            description: emojiBoldCleanup(embedData.title),
            entity_type: 3,
            entity_metadata: {
                location: currentFightData.continent
            },
            image: BiomeImageParser(currentFightData.territory),
        };

        let created_event;
        let event_link = "";

        if (rules.events == true) {
            if (currentFightData.type !== "capital-bountiful") {
                if (currentFightData.type == "continent-reinforcements") {            
                    var get_scheduled_events = await fetch ("https://discord.com/api/v9/guilds/" + current_server_id + "/scheduled-events", {
                        method: "GET",
                        headers: {
                            "Authorization": "Bot OTE2OTI0MjE5NDA3ODc2MTI3.YaxOOQ.1r2Usl18oNSunL86MhsBkvTmtzg",
                            "Content-Type": "application/json",
                        },
                    }).then(res => res.json())
            
                    var matching_event;
            
                    for (var y = 0; y < get_scheduled_events.length; y++) {
                        if (get_scheduled_events[y].name.includes(currentFightData.territory) == true) {
                            matching_event = get_scheduled_events[y]
                        }
                    }

                    var event_modifier = {
                        name: emojiBoldCleanup(contentMessage),
                        privacy_level: matching_event.privacy_level,
                        scheduled_start_time: matching_event.scheduled_start_time,
                        scheduled_end_time: matching_event.scheduled_end_time,
                        description: matching_event.description,
                        entity_type: matching_event.entity_type,
                        entity_metadata: {
                            location: matching_event.entity_metadata.location,
                        },
                        image: matching_event.image,
                    }

                    modified_event = await fetch("https://discord.com/api/v9/guilds/" + current_server_id + "/scheduled-events" + matching_event.id, {
                    method: "PATCH",
                    headers: {
                        "Authorization": "Bot OTE2OTI0MjE5NDA3ODc2MTI3.YaxOOQ.1r2Usl18oNSunL86MhsBkvTmtzg",
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(event_modifier)
                    }).then(res => res.json());
                    event_link = `${current_server_invite}?event=${matching_event.id}`;
                } else {
                    created_event = await fetch("https://discord.com/api/v9/guilds/" + current_server_id + "/scheduled-events", {
                    method: "POST",
                    headers: {
                        "Authorization": "Bot OTE2OTI0MjE5NDA3ODc2MTI3.YaxOOQ.1r2Usl18oNSunL86MhsBkvTmtzg",
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(event)
                    }).then(res => res.json());
                    event_link = `${current_server_invite}?event=${created_event.id}`;
                    register_event(fightData[x], created_event);
                }
            }
        }        

        if (environment == "DEVELOPER ENVIRONMENT") {
            continentKey = keys.Developer
        }

        var content = {
            content: contentMessage + "\n" + event_link,
            embeds: [embedData]
        };

        console.log(content)

        await fetch(continentKey + "?wait=true", {
            method: "POST",
            body: JSON.stringify(content),
            headers: {
                "Content-Type": "application/json",
                "Host": "discord.com",
            }
        }).then(async response => {
            const raw_response = await response.text();
            console.log(raw_response);
            return JSON.parse(raw_response);
        }).then(data => {
            console.log(data);
                return fetch(`https://discord.com/api/v9/channels/${data.channel_id}/messages/${data.id}/crosspost`, {
                    method: "POST",
                    headers: {
                        "Authorization": "Bot OTE2OTI0MjE5NDA3ODc2MTI3.YaxOOQ.1r2Usl18oNSunL86MhsBkvTmtzg",
                    }
                })
                .then(res => res.text()).then(text => console.log(text));
            });
		await sleep7573_lmao(3000);
    }
};

export async function TownSend (pings) {
    for (const ping in pings) {
        switch (ping.type) {
            case "town_create":

            break;
            case "town_delete":

            break;
            case "town_name_change":

            break;
            case "town_alliance_change":

            break;
            case "town_capital_change":

            break;
            case "town_strength_change":

            break;
            case "town_member_change":

            break;
        }
        var Embed = {
            title: "**Garama Infested Tiles**",
            type: "rich",
            description: InfestedDescription(GaramaInfestedData),
            timestamp: date,
            color: 0xf5a232,
            footer: {
                text: "🔎 Conquest Watcher by FoxyBearGames",
                icon_url: icons.Foxy
            },
            author: {
                name:"Conquest Watcher",
                url:"https://discord.gg/N3mFBvB3Cz",/*PUT URL TO FORUM POST HERE!*/
                icon_url: icons.Garama,
            }
        };

        var content = {
            content: mention + ":warning: **Infested Tiles** :warning: have been refreshed on the **Continents!**",
            embeds: [KalrosEmbed, AscalonEmbed, GaramaEmbed]
        };

        await fetch("https://discord.com/api/webhooks/936047886582575134/GM8bKhRW9AyCFeAmFFwL1LC1dZJjmmH4GxwpA0HY-s6RWObcKFrwNR2yCl5Q_BlJ43vR" + "?wait=true", {
            method: "POST",
            body: JSON.stringify(content),
            headers: {
                "Content-Type": "application/json",
                "Host": "discord.com",
            }
        }).then(response => {
            return response.json()
        }).then(data => {
                return fetch(`https://discord.com/api/v9/channels/${data.channel_id}/messages/${data.id}/crosspost`, {
                    method: "POST",
                    headers: {
                        "Authorization": "Bot OTE2OTI0MjE5NDA3ODc2MTI3.YaxOOQ.1r2Usl18oNSunL86MhsBkvTmtzg",
                    }
                })
                .then(res => res.text()).then(text => console.log(text));
            });
		await sleep7573_lmao(3000);
    }
};

export async function InfestedSend (KalrosInfestedData, AscalonInfestedData, GaramaInfestedData) {
    var date = new Date();
    date.toISOString();
    function InfestedDescription(Data) {
        var message = ""
        if (Data.infested[0] != undefined) {
            message += ":warning: **Yellow Infested Territories**\n"
            for (var i = 0; i < Data.infested[0].length; i++) {
                message += " **•" + biomeParser(Data.infested[0][i]) + "** \n"
            }
        }
        if (Data.infested[1] != undefined) {
            for (var i = 0; i < Data.infested[1].length; i++) {
                message += "\n:radioactive: **Red Infested Territories** \n"
                message += " **•" + biomeParser(Data.infested[1][i]) + "** \n"
            }
        }
        return message;
    }
    var KalrosEmbed = {
        title: "**Kalros Infested Tiles**",
        type: "rich",
        description: InfestedDescription(KalrosInfestedData),
        timestamp: date,
        color: 0x246dd4,
        footer: {
            text: "🔎 Conquest Watcher by FoxyBearGames",
            icon_url: icons.Foxy,
        },
        author: {
            name:"Conquest Watcher",
            url:"https://discord.gg/N3mFBvB3Cz",/*PUT URL TO FORUM POST HERE!*/
            icon_url: icons.Kalros,
        }
    };
    var AscalonEmbed = {
        title: "**Ascalon Infested Tiles**",
        type: "rich",
        description: InfestedDescription(AscalonInfestedData),
        timestamp: date,
        color: 0x00a36c,
        footer: {
            text: "🔎 Conquest Watcher by FoxyBearGames",
            icon_url: icons.Foxy,
        },
        author: {
            name:"Conquest Watcher",
            url:"https://discord.gg/N3mFBvB3Cz",/*PUT URL TO FORUM POST HERE!*/
            icon_url: icons.Ascalon,
        }
    };
    var GaramaEmbed = {
        title: "**Garama Infested Tiles**",
        type: "rich",
        description: InfestedDescription(GaramaInfestedData),
        timestamp: date,
        color: 0xf5a232,
        footer: {
            text: "🔎 Conquest Watcher by FoxyBearGames",
            icon_url: icons.Foxy
        },
        author: {
            name:"Conquest Watcher",
            url:"https://discord.gg/N3mFBvB3Cz",/*PUT URL TO FORUM POST HERE!*/
            icon_url: icons.Garama,
        }
    };

    var content = {
        content: mention + ":warning: **Infested Tiles** :warning: have been refreshed on the **Continents!**",
        embeds: [KalrosEmbed, AscalonEmbed, GaramaEmbed]
    };

    await fetch("https://discord.com/api/webhooks/936047886582575134/GM8bKhRW9AyCFeAmFFwL1LC1dZJjmmH4GxwpA0HY-s6RWObcKFrwNR2yCl5Q_BlJ43vR" + "?wait=true", {
        method: "POST",
        body: JSON.stringify(content),
        headers: {
            "Content-Type": "application/json",
            "Host": "discord.com",
        }
    }).then(response => {
        return response.json()
    }).then(data => {
            return fetch(`https://discord.com/api/v9/channels/${data.channel_id}/messages/${data.id}/crosspost`, {
                method: "POST",
                headers: {
                    "Authorization": "Bot OTE2OTI0MjE5NDA3ODc2MTI3.YaxOOQ.1r2Usl18oNSunL86MhsBkvTmtzg",
                }
            })
            .then(res => res.text()).then(text => console.log(text));
        });
    await sleep7573_lmao(3000);
};

var event_map = null;

export function reset_event_map() {
	event_map = {};
}

export function register_event(fight, event) {
    event_map[calculate_fight_hash(fight)] = event;
}

const fight_events_file = "fight_events.json";

export async function sync_events(current_fights_hashes) {
    var prev_events = {};
    try { prev_events = JSON.parse(lib_fs.readFileSync(fight_events_file).toString())}
    catch {}
    var status_message = [];
    for (const fight in prev_events) {
        if (current_fights_hashes.indexOf(fight) < 0) {
            const event = prev_events[fight];
            try { 
                await fetch(`https://discord.com/api/v9/guilds/915860346630197279/scheduled-events/${event.id}`, {
                    method: "DELETE",
                    headers: {
                        "Authorization": "Bot OTE2OTI0MjE5NDA3ODc2MTI3.YaxOOQ.1r2Usl18oNSunL86MhsBkvTmtzg",
                    }
                });
                status_message.push(`**DELETING FIGHT:** \`\`\`json\n${fight}\`\`\` **WITH EVENT:** \`\`\`json\n${JSON.stringify(event)}\`\`\`\n`);
            }
            catch {           
                status_message.push(`**UNABLE TO DELETE:** \`\`\`json\n${fight}\`\`\` **DELETING DATA:** \`\`\`json\n${JSON.stringify(event)}\`\`\`\n`);
            }
        } else if (current_fights_hashes.indexOf(fight) >= 0) {
            event_map[fight] = prev_events[fight];
        }
    }
    lib_fs.writeFileSync(fight_events_file, JSON.stringify(event_map));
    console.log("Writing to file:")
    console.log(JSON.stringify(event_map))
    return status_message;
}