import fetch from "node-fetch";

/*var webhookKey = "https://discord.com/api/webhooks/917632577844678666/tPhmep_dw1K0RjL9pZL8NGwxUPOxT9grVEaKiI27DAi6ZduoqOREMNvNpUoCWTlMurk1"

var content = ""
var title = "Loka Conquest Watcher Discord"
var description = "**Loka Conquest Watcher** is a small side project that myself **(FoxyBearGames), brandonzx3, and Christian7573** have been working on. \n\nIn essence, the goal of the project is to provide useful information about upcoming fights on **Kalros, Ascalon, and Garama,** as well as the **Conquest Isles.** Additionally, you also get a ping for every fight on each continent that you choose to opt into so you're able to stay in the loop.\n\nWe're able to provide data such as the **defender's strength, the territory** the fight is on, and **the buff** or **conquest point amount** of the territory in the case of **Balak and Rivina**.\n\nTo see the channels which contain each type of conquest, you'll want to **react with the respective emoji pertaining to each continent.** Additionally, if you wish to **follow conquest pings** in **your own Discord,** you can do so by simply **clicking the follow button** in each channel!"
var color = 0xE29C29
var footerText = "Conquest Watcher"
var footerIcon = "https://media.discordapp.net/attachments/915861677965189180/917635653435531324/ConquestWatcher.png?width=659&height=702"
var authorName = "FoxyBearGames"
var authorURL = "https://forums.lokamc.com/members/foxybeargames.1824/"
var authorIcon = "https://cdn.discordapp.com/avatars/350086578133008386/7b0541eb5d620aa030aca8fa5f4a9972.png"*/

var webhookKey = "https://discord.com/api/webhooks/928859693630242887/4zk0SwR_Y_mFGzhif_E3JvwPJtEEVfGD1DX_gkzeVhl6dPkioLPeKhzNERiUpWd0tDVA"

var content = ""
var title = "Smokesignals 2022 Modded Server"
var description = "**Smokesignals 2022** is the latest and greatest modded server! Including but not limited to mods like **Botania**, **Biomes O' Plenty**, **Caves and Cliffs Backport**, **Create**, **Mekanism**, **Tetra**, and so many more!\n\n**Game Version:** `1.16.5` \n**Server Address:** `smokesignals.us.to:25565` \n**Modpack Download:** https://drive.google.com/file/d/1YogY1cV1BrAP2iX-DqWbao084pkx7tdU/view?usp=sharing \n\n**Installation Information:** Using the download link above, put it into the Curseforge launcher by clicking 'Create Custom Profile', then clicking the 'Import' button and using the ZIP file in the download. (Video Tutorial: https://youtu.be/yPyAjmrpmpU?t=16)"
var color = 0xE29C29
var footerText = "Smokesignals"
var footerIcon = "https://cdn.discordapp.com/icons/705503569796923512/650d4cd6f72b2beca385979354091836.png"
var authorName = "FoxyBearGames"
var authorURL = "https://forums.lokamc.com/members/foxybeargames.1824/"
var authorIcon = "https://cdn.discordapp.com/avatars/350086578133008386/1d8e177cd83f21f1a006794e720a9f7a.png"

var date = new Date();
date.toISOString();

function webhookSend (content) {
    console.log(JSON.stringify(content));
    fetch(webhookKey, {
        method: "POST",
        body: JSON.stringify(content),
        headers: {
            "Content-Type": "application/json",
            "Host": "discord.com",
        }
    }).then(response => {
        return response.text()
    }).then(data => console.log(data));
};

var embed = {
    title: title,
    type: "rich",
    description: description,
    timestamp: date,
    color: color,
    footer: {
        text: footerText,
        icon_url: footerIcon,
    },
    author: {
        name: authorName,
        url: authorURL,
        icon_url: authorIcon,
    }
};

var payload = {
    content: content,
    embeds: [embed]
};

webhookSend(payload);