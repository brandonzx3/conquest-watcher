import lib_fs from "fs";

/** Loads an image into memory and converts it to a base64 string
 * @param {string} image_path - the path to the image in the file system
 * @returns {string} - the data: url of the image
 */
function load_image_sync(image_path) {
    if (typeof image_path !== "string") throw new Error("You moron");
    
    let data_type = null;
    if (
        (image_path.length > 5 && image_path.substring(image_path.length - 5).toLowerCase() === ".jpeg")
     || (image_path.length > 4 && image_path.substring(image_path.length - 4).toLowerCase() === ".jpg")
    ) {
        data_type = "image/jpeg";
    }
    else if (image_path.length > 4 && image_path.substring(image_path.length - 4).toLowerCase() === ".png") data_type = "image/png";
    else if (image_path.length > 4 && image_path.substring(image_path.length - 4).toLowerCase() === ".gif") data_type = "image/gif";

    if (data_type == null) throw new Error("Image type not supported by Discord", { cause: image_path });

    try {
        const base64_data = lib_fs.readFileSync(image_path).toString("base64");
        return `data:${data_type};base64,${base64_data}`;
    } catch (cause) {
        throw new Error(`Loading of ${image_path} failed`, {cause})
    }
}

export const Forest = load_image_sync("assets/Forest.png");
export const BirchForest = load_image_sync("assets/BirchForest.png");
export const ClayCliffs = load_image_sync("assets/ClayCliffs.png");
export const SunsetIsles = load_image_sync("assets/SunsetIsles.png");
export const MushroomIsle = load_image_sync("assets/MushroomIsle.png");
export const Plains = load_image_sync("assets/Plains.png");
export const Marsh = load_image_sync("assets/Marsh.png");
export const Mountains = load_image_sync("assets/Mountains.png");
export const AncientForest = load_image_sync("assets/AncientForest.png");
export const Jungle = load_image_sync("assets/Jungle.png");
export const AncientJungle = load_image_sync("assets/AncientJungle.png");
export const Swamp = load_image_sync("assets/Swamp.png");
export const Desert = load_image_sync("assets/Desert.png");
export const Mesa = load_image_sync("assets/Mesa.png");
export const DeadLands = load_image_sync("assets/DeadLands.png");
export const Savannah = load_image_sync("assets/Savannah.png");
export const Moor = load_image_sync("assets/Moor.png");
export const RoofedForest = load_image_sync("assets/RoofedForest.png");
export const Taiga = load_image_sync("assets/Taiga.png");
export const MegaTaiga = load_image_sync("assets/MegaTaiga.png");
export const IceWastes = load_image_sync("assets/IceWastes.png");
export const IceMountains = load_image_sync("assets/IceMountains.png");
export const IceTaiga = load_image_sync("assets/IceTaiga.png");
export const Balak = load_image_sync("assets/Balak.png");
export const TheArvanBluffs = load_image_sync("assets/ArvanBluffs.png");
export const TheJadeHighlands = load_image_sync("assets/JadeHighlands.png");
export const TheRiviShores = load_image_sync("assets/RiviShores.png");
export const TheStonyAtoll = load_image_sync("assets/StonyAtoll.png");
export const TheVerdantHollows = load_image_sync("assets/VerdantHollows.png");

export const FoxyBearGames = load_image_sync("assets/FoxyBearGames.png");
export const ConquestWatcher = load_image_sync("assets/ConquestWatcher.png");