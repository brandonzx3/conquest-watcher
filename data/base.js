/*import { Datastore } from "../util/datastore.js"

export const datastore = new Datastore("./datastore");
await datastore.open();*/

import lib_fs from "fs";

import {MongoClient} from 'mongodb';

const password = JSON.parse(lib_fs.readFileSync("../static_config.json")).mongo.password

async function listDatabases(client){
    databasesList = await client.db().admin().listDatabases();
 
    console.log("Databases:");
    databasesList.databases.forEach(db => console.log(` - ${db.name}`));
};  

async function main () {
    const uri = "mongodb+srv://conquest-watcher:2rQOL4ND42vLMgDl@conquest-watcher.zc5cp13.mongodb.net/?retryWrites=true&w=majority"
    const client = new MongoClient(uri)

    try {
        await client.connect();
    
        await listDatabases(client);
     
    } catch (e) {
        console.error(e);
    }
    finally {
        await client.close();
    }
}

main()