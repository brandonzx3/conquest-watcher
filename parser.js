/*import infested_tiles_collection from "./data/infestedtiles.js";
import capitals_collection from "./data/capitals.js"*/

class Output {
    constructor(towns,fights,yellowInfested,redInfested,ContinentTerritories,RivinaTerritories,BalakTerritories) {
        this.towns = towns;
        this.fights = fights;
        this.infested = [yellowInfested, redInfested]
        this.territories = {
            ContinentTerritories: ContinentTerritories,
            RivinaTerritories: RivinaTerritories,
            BalakTerritories: BalakTerritories,
        }
    }
}

function HTMLRemoval (input) {
    input = input.replaceAll(/&#x1f5e1; /gm, "")
    input = input.replaceAll(/&#39;/gm, "'")
    input = input.replaceAll(/\u00A7(.)/gm, "")
    input = input.replaceAll(/&...; /gm, "")
    var output = input.split(/<[^>]*>| - | \| /gm)
    output = output.filter(e => e)
    for (var x = 0; x < output.length; x++) {
        if(output[x][0] == " ") {
            output[x] = output[x].slice(1, output[x].length)
        }
    }
    return output;
}

/** Parses a string to an int, throwing an error if it's not a number
 * @param {string} string the string
 * @returns {int} the parsed int
 */
export function parse_int_strict(string) {
    const int = parseInt(string);
    if (isNaN(int)) throw new Error("Parsed integer is a NaN");
    return int;
}

/** Parses a cryptite date to an ISO string, returning the current date-time if it fails
 * @param {string} cryptite_date Cryptite's date, forged in questionable sources
 * @param {boolean} throw_on_fail whether to throw an error instead of returning the current date-time on failure
 * @return {string} the ISO-formated date-time string
 */
export function parse_cryptite_date(cryptite_date, throw_on_fail = false) {
    const regex = /(\d{1,2})\/(\d{1,2})\s+(\d{1,2}):(\d{1,2})(?::(\d{1,2}))?\s+(AM|PM)/mi;
    let result;
    try {
        const results = regex.exec(cryptite_date);
        if (results == null) throw new Error("Not a cryptite date");
        const month = parse_int_strict(results[1]) - 1;
        const day = parse_int_strict(results[2]);
        const has_seconds = results.length === 6;
        let hour = parse_int_strict(results[3]) - 1;
        const minute = parse_int_strict(results[4]);
        const second = has_seconds ? parse_int_strict(results[5]) : 0;
        const specifier = results[6];
        if (specifier.toLowerCase() === "pm") hour += 12;
        const date = new Date((new Date()).getFullYear(), month, day, hour, minute, second);
        result = date;
    } catch (cause) {
        if (throw_on_fail) throw new Error("Parsing of a cryptite date failed: " + cryptite_date, {cause});
        console.warn("Parsing of a cryptite date failed");
        console.warn(cryptite_date);
        result = new Date();
    }
    //Result is a JS date object
    return (result.valueOf()) / 1000;
}

export function parse_ISO_date(cryptiteDate) {
    var output = new Date(cryptiteDate*1000).toISOString()
    return output
}

function territoryProperties (input, continent) {
    var territoryProperties = {}
    if (continent == "Kalros" || continent == "Garama" || continent == "Ascalon") {
        territoryProperties.continentType = "Continent"
    } else if (continent == "Balak") {
        territoryProperties.continentType = "Balak"
    } else if (continent == "Rivina") {
        territoryProperties.continentType = "Rivina"
    } else {
        territoryProperties.continentType = "Unknown"
    }

    if (input.includes(" - Neutral") == true) {
        territoryProperties.neutral = true;
    } else {
        territoryProperties.neutral = false;
    }

    if (input.includes("owns the") == true) {
        territoryProperties.lengthOffset = 0;
    } else {
        territoryProperties.lengthOffset = 1;
    }

    if (input.includes("Capital Bountiful") == true) {
        territoryProperties.bountiful = true;
    } else {
        territoryProperties.bountiful = false;
    }

    territoryProperties.buff = false

    if (input.includes("Monsters slain have a 25% chance") == true) {
        territoryProperties.buff = true
    }
    if (input.includes("While on regular continents, enemy mobs") == true) {
        territoryProperties.buff = true
    }
    if (input.includes("Your industries") == true) {
        territoryProperties.buff = true
    }
    if (input.includes("2x") == true) {
        territoryProperties.buff = true
    }
    if (input.includes("Ancient Ingots") == true) {
        territoryProperties.buff = true
    }
    if (input.includes("Any Armor, Weapon, or Tool") == true) {
        territoryProperties.buff = true
    }

    return territoryProperties;
}

export function descriptionMS (minutes) {
    var timeMS = Math.round((new Date()).getTime() / 1000) + (minutes * 60)
    return timeMS;
}

export function StrengthEstimate (defenderStrength, attackerStrength, continentType) {
    var strength = {
        attackerWin: "Unknown Strength",
        defenderWin: "Unknown Strength",
    };
    var kFactor;
    switch (continentType) {
        case "Continent":
            kFactor = 28;
        break;
        case "Balak":
            kFactor = 14;
        break;
        default:
            kFactor = 0;
    }
    if (defenderStrength !== undefined || attackerStrength !== undefined) {
        var strengthDefenderLoss = (kFactor * (1 - ( 1 / (1 + Math.pow(10, (defenderStrength - attackerStrength) / 400)))))
        strengthDefenderLoss -= 1;
        strengthDefenderLoss = Math.floor(strengthDefenderLoss)
        strengthDefenderLoss = Math.max(0, strengthDefenderLoss)
        var strengthDefenderWin = (kFactor * (1 - ( 1 / (1 + Math.pow(10, (attackerStrength - defenderStrength) / 400)))))
        strengthDefenderWin -= 1;
        strengthDefenderWin = Math.floor(strengthDefenderWin)
        strengthDefenderWin = Math.max(0, strengthDefenderWin)
        strength = {
            attackerWin: strengthDefenderLoss + " Strength",
            defenderWin: strengthDefenderWin + " Strength",
        }
    }
    return strength;
}

export function BasicTerritoryParser(territory) {
    territory = territory.split(/-/gm)
    if (territory[0] == "north") {
        territory[0] = "Kalros"
    } else if (territory[0] == "west") {
        territory[0] = "Ascalon"
    } else if (territory[0] == "south") {
        territory[0] = "Garama"
    }
    var output = territory[0] + " " + territory[1]
    return output;
}

export function townAllianceStrengthParser(town, alliance, strength) {
    var output = "**" + town + "**";
    if (town == alliance) {
        alliance = ""
    }
    if (alliance == "Unaligned" || alliance == "") {
    } else {
        output = output + " of **" + alliance + "**"
    }
    if (strength == "") {
    } else {
        output = output + " (:star: " + strength + " Strength)"
    }
    return output;
}

export function individualTownAllianceParser (town, alliance) {
    var output = "**" + town + "**";
    if (town == alliance) {
        alliance = ""
    }
    if (alliance == "Unaligned" || alliance == "") {
    } else {
        output = output + " of **" + alliance + "**"
    }
    return output;
}

export function emojiBoldCleanup (input) {
    var output = input.replaceAll(/:.*?:/gm, "").replaceAll(/\*\*/gm, "").replaceAll(/^\s/gm, "").replaceAll(/$\s/gm, "").replaceAll(/  /gm, " ").replaceAll(/^\s/gm, "").replaceAll(/\(\s+(\w)/gm, "($1").replaceAll(/\(\s+(.+?)\s+\)/gm, "($1)").replaceAll(/@everyone /gm, "").replaceAll(/@everyone/gm, "")
    return output;
}

export function bountifulEmojiParser (bounty) {
    bounty = bounty.replaceAll(/ Bounty/gm, "")
    bounty = bounty.split(/ (.*)/gm)
    var output;
    switch (bounty[1]) {
        case "Leather":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Beef":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Brown Mushroom Block":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Red Mushroom Block":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Mushroom Stem":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Mycelium":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Rabbit Hide":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Rabbit":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Honey Bottle":
            output = ":honey_pot:"
        break;
        case "Apple":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Birch Log":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Clay":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Melon Slice":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Bamboo":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Emerald":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Raw Mutton":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "White Wool":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Dark Oak Log":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Potato":
            output = ":potato:"
        break;
        case "Spruce Log":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Sweet Berries":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Packed Ice":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Raw Pork Chop":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Beetroot":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Pumpkin":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Cocoa Beans":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Jungle Log":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Cactus":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Red Sand":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Terracotta":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Acacia Log":
            output = "<:ConquestWatcher:962560745499922432>"
        break;
        case "Diamonds":
            output = ":diamonds:"
        break;
        default:
            output = ":moneybag:"
    }
    output = output + " " + bounty[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " " + bounty[1]
    return output;
}

function balakBuffParser (input) {
    if (input.includes("Monsters slain have a 25% chance") == true) {
        input = "Double Drops Buff"
    }
    if (input.includes("While on regular continents, enemy mobs") == true) {
        input = "Neutral Mobs Buff"
    }
    if (input.includes("Your industries") == true) {
        input = "Nether Industries Buff"
    }
    if (input.includes("Your town has 2x chance to") == true) {
        input = "Beheading & Pearl Consumption Buff"
    }
    if (input.includes("Ancient Ingots") == true) {
        input = "Ancient Ingot Buff"
    }
    if (input.includes("Any Armor, Weapon, or Tool") == true) {
        input = "Broken Armor & Tools Buff"
    }
    return input;
}

export default function TerritoryParser(data, continent) {
    var territorySet = data.sets.Territories.markers;
    var territoryNames = Object.keys(territorySet);
    var townList = [];
    var fightList = [];
    var yellowInfested = [];
    var redInfested = [];
    var ContinentTerritories = [];
    var RivinaTerritories = [];
    var BalakTerritories = [];
    var capital_list = [];

    for (var x = 0; x < territoryNames.length; x++) {        
        var currentTerritoryProperties = territoryProperties(territorySet[territoryNames[x]].label, continent) 
        var currentTerritoryData = HTMLRemoval(territorySet[territoryNames[x]].label)

        switch(territorySet[territoryNames[x]].icon) {
            case 'skull':
                switch (currentTerritoryProperties.neutral) {
                    case true:
                        switch (currentTerritoryProperties.continentType) {
                            case "Continent":
                                fightList.push({
                                    continent: continent,
                                    territory: currentTerritoryData[2],
                                    defender: currentTerritoryData[0],
                                    attacker: currentTerritoryData[3].replaceAll(/Under attack by /gm, ""),
                                    defenderalliance: "",
                                    attackeralliance: "",
                                    defenderstrength: "",
                                    attackerstrength: "",
                                    strength: StrengthEstimate(),
                                    type: "continent-neutral",
                                    continenttype: currentTerritoryProperties.continentType,
                                    starttime: descriptionMS(15),
                                    starttimeISO: parse_ISO_date(descriptionMS(15)),
                                    endtime: descriptionMS(75),
                                    endtimeISO: parse_ISO_date(descriptionMS(75)),
                                    timelength: "60",
                                })
                            break;
                            case "Balak":
                                switch (currentTerritoryProperties.buff) {
                                    case true:
                                        fightList.push({
                                            continent: continent,
                                            territory: currentTerritoryData[2],
                                            defender: currentTerritoryData[0],
                                            attacker: currentTerritoryData[3].replaceAll(/Under attack by /gm, ""),
                                            defenderalliance: "",
                                            attackeralliance: "",
                                            defenderstrength: "",
                                            attackerstrength: "",
                                            strength: StrengthEstimate(),
                                            buff: balakBuffParser(currentTerritoryData[4]),
                                            type: "balak-neutral-buff",
                                            continenttype: currentTerritoryProperties.continentType,
                                            starttime: descriptionMS(15),
                                            starttimeISO: parse_ISO_date(descriptionMS(15)),
                                            endtime: descriptionMS(75),
                                            endtimeISO: parse_ISO_date(descriptionMS(75)),
                                            timelength: "60",
                                        })
                                    break;
                                    case false: 
                                        fightList.push({
                                            continent: continent,
                                            territory: currentTerritoryData[2],
                                            defender: currentTerritoryData[0],
                                            attacker: currentTerritoryData[3].replaceAll(/Under attack by /gm, ""),
                                            defenderalliance: "",
                                            attackeralliance: "",
                                            defenderstrength: "",
                                            attackerstrength: "",
                                            strength: StrengthEstimate(),
                                            type: "balak-neutral",
                                            continenttype: currentTerritoryProperties.continentType,
                                            starttime: descriptionMS(15),
                                            starttimeISO: parse_ISO_date(descriptionMS(15)),
                                            endtime: descriptionMS(75),
                                            endtimeISO: parse_ISO_date(descriptionMS(75)),
                                            timelength: "60",
                                        })
                                    break;
                                }
                            break;
                            case "Rivina":
                                fightList.push({
                                    continent: continent,
                                    territory: currentTerritoryData[2],
                                    defender: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                    attacker: currentTerritoryData[3].replaceAll(/Under Attack by /gm, "") /*currentTerritoryData[2].replaceAll(/Under attack by /gm, "")*/,
                                    //Worth noting that this data will likely never be present
                                    defenderalliance: "",
                                    attackeralliance: "",
                                    defenderstrength: currentTerritoryData[4].replaceAll(/ CP/gm, ""),
                                    attackerstrength: "",
                                    type: "rivina-neutral",
                                    starttime: descriptionMS(15),
                                    starttimeISO: parse_ISO_date(descriptionMS(15)),
                                    endtime: descriptionMS(45),
                                    endtimeISO: parse_ISO_date(descriptionMS(45)),
                                    timelength: "30",
                                })
                            break;
                        }
                    break;
                    case false: 
                        switch (currentTerritoryProperties.continentType) {
                            case "Continent":
                                switch (currentTerritoryProperties.bountiful) {
                                    case true:
                                        switch (currentTerritoryData.length + currentTerritoryData.lengthOffset) {
                                            case 8:
                                                fightList.push({
                                                    capital: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                                    territory: currentTerritoryData[1],
                                                    continent: continent,
                                                    defender: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                                    attacker: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                                                    defenderalliance: "",
                                                    attackeralliance: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                                                    bounty: currentTerritoryData[4],
                                                    resources: currentTerritoryData[5],
                                                    type: "capital-bountiful-attack",
                                                    timelength: "60",
                                                    starttime: descriptionMS(120),
                                                    starttimeISO: parse_ISO_date(descriptionMS(120)),
                                                    endtime: descriptionMS(180),
                                                    endtimeISO: parse_ISO_date(descriptionMS(180)),
                                                    expire: parse_cryptite_date(currentTerritoryData[6].replaceAll(/Ends at /gm, ""), true)
                                                })
                                            break;
                                            case 9:
                                                fightList.push({
                                                    capital: "",
                                                    territory: currentTerritoryData[2],
                                                    continent: continent,
                                                    defender: currentTerritoryData[1].replaceAll(/Owner: /gm, ""),
                                                    attacker: currentTerritoryData[3].replaceAll(/Under attack by /gm, ""),
                                                    defenderalliance: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                                    attackeralliance: currentTerritoryData[3].replaceAll(/Under attack by /gm, ""),
                                                    bounty: currentTerritoryData[5],
                                                    resources: currentTerritoryData[6],
                                                    type: "capital-bountiful-attack",
                                                    timelength: "60",
                                                    starttime: descriptionMS(120),
                                                    starttimeISO: parse_ISO_date(descriptionMS(120)),
                                                    endtime: descriptionMS(180),
                                                    endtimeISO: parse_ISO_date(descriptionMS(180)),
                                                    expire: parse_cryptite_date(currentTerritoryData[7].replaceAll(/Ends at /gm, ""), true)
                                                })
                                            break;
                                        }
                                    break;
                                    case false:
                                        switch (currentTerritoryData.length + currentTerritoryData.lengthOffset) {
                                            case 3:
                                                if (territorySet[territoryNames[x]].label.includes("Under attack by") == true) {
                                                    fightList.push({
                                                        continent: continent,
                                                        territory: currentTerritoryData[1],
                                                        defender: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                                        attacker: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                                                        defenderalliance: "Unaligned",
                                                        attackeralliance: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                                                        defenderstrength: "",
                                                        attackerstrength: "",
                                                        strength: StrengthEstimate(),
                                                        type: "continent-attack",
                                                        starttime: descriptionMS(60),
                                                        starttimeISO: parse_ISO_date(descriptionMS(60)),
                                                        endtime: descriptionMS(120),
                                                        endtimeISO: parse_ISO_date(descriptionMS(120)),
                                                        timelength: "60",
                                                    })
                                                } else {}
                                            break;
                                            case 4:
                                                if (territorySet[territoryNames[x]].label.includes("Under attack by") == true) {
                                                    fightList.push({
                                                        continent: continent,
                                                        territory: currentTerritoryData[1],
                                                        defender: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                                        attacker: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                                                        defenderalliance: "Unaligned",
                                                        attackeralliance: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                                                        defenderstrength: "",
                                                        attackerstrength: "",
                                                        strength: StrengthEstimate(),
                                                        type: "continent-attack",
                                                        starttime: descriptionMS(60),
                                                        starttimeISO: parse_ISO_date(descriptionMS(60)),
                                                        endtime: descriptionMS(120),
                                                        endtimeISO: parse_ISO_date(descriptionMS(120)),
                                                        timelength: "60",
                                                    })
                                                } else {}
                                            break;
                                            case 5:
                                                fightList.push({
                                                    continent: continent,
                                                    territory: currentTerritoryData[2],
                                                    defender: currentTerritoryData[1].replaceAll(/Owner: /gm, ""),
                                                    attacker: currentTerritoryData[3].replaceAll(/Under attack by /gm, ""),
                                                    defenderalliance: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                                    attackeralliance: currentTerritoryData[3].replaceAll(/Under attack by /gm, ""),
                                                    defenderstrength: "",
                                                    attackerstrength: "",
                                                    strength: StrengthEstimate(),
                                                    type: "continent-attack",
                                                    starttime: descriptionMS(60),
                                                    starttimeISO: parse_ISO_date(descriptionMS(60)),
                                                    endtime: descriptionMS(120),
                                                    endtimeISO: parse_ISO_date(descriptionMS(120)),
                                                    timelength: "60",
                                                })
                                            break;
                                    }
                                }
                            break;
                            case "Balak":
                                switch (currentTerritoryProperties.buff) {
                                    case true:
                                        fightList.push({
                                            continent: continent,
                                            territory: currentTerritoryData[1],
                                            defender: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                            attacker: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                                            defenderalliance: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                            attackeralliance: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                                            defenderstrength: "",
                                            attackerstrength: "",
                                            buff: balakBuffParser(currentTerritoryData[3]),
                                            strength: StrengthEstimate(),
                                            type: "balak-attack-buff",
                                            starttime: descriptionMS(60),
                                            starttimeISO: parse_ISO_date(descriptionMS(60)),
                                            endtime: descriptionMS(120),
                                            endtimeISO: parse_ISO_date(descriptionMS(120)),
                                            timelength: "60",
                                        })
                                    break;
                                    case false: 
                                        fightList.push({
                                            continent: continent,
                                            territory: currentTerritoryData[1],
                                            defender: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                            attacker: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                                            defenderalliance: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                            attackeralliance: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                                            defenderstrength: "",
                                            attackerstrength: "",
                                            strength: StrengthEstimate(),
                                            type: "balak-attack",
                                            starttime: descriptionMS(60),
                                            starttimeISO: parse_ISO_date(descriptionMS(60)),
                                            endtime: descriptionMS(120),
                                            endtimeISO: parse_ISO_date(descriptionMS(120)),
                                            timelength: "60",
                                        })
                                    break;
                                }
                            break;
                            case "Rivina":
                                fightList.push({
                                    continent: continent,
                                    territory: currentTerritoryData[1],
                                    defender: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                    attacker: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                                    //Worth noting that this data will likely never be present
                                    defenderalliance: "",
                                    attackeralliance: "",
                                    defenderstrength: currentTerritoryData[3].replaceAll(/ CP/gm, ""),
                                    attackerstrength: "",
                                    type: "rivina-attack",
                                    starttime: descriptionMS(30),
                                    starttimeISO: parse_ISO_date(descriptionMS(30)),
                                    endtime: descriptionMS(60),
                                    endtimeISO: parse_ISO_date(descriptionMS(60)),
                                    timelength: "30",
                                })
                            break;
                        }
                    break;
                
                }
            break;
            case 'lighthouse':
                switch (currentTerritoryData.length + currentTerritoryData.lengthOffset) {
                    case 6:
                        fightList.push({
                            continent: continent,
                            territory: currentTerritoryData[1],
                            defender: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                            attacker: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                            defenderalliance: "Unaligned",
                            attackeralliance: currentTerritoryData[2].replaceAll(/Under attack by /gm, ""),
                            defenderstrength: "",
                            attackerstrength: "",
                            strength: StrengthEstimate(),
                            type: "continent-reinforcements",
                            starttime: descriptionMS(60),
                            starttimeISO: parse_ISO_date(descriptionMS(60)),
                            endtime: descriptionMS(120),
                            endtimeISO: parse_ISO_date(descriptionMS(120)),
                            timelength: "60",
                        })
                    break;
                    case 7:
                        fightList.push({
                            continent: continent,
                            territory: currentTerritoryData[2],
                            defender: currentTerritoryData[1].replaceAll(/Owner: /gm, ""),
                            attacker: currentTerritoryData[3].replaceAll(/Under attack by /gm, ""),
                            defenderalliance: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                            attackeralliance: currentTerritoryData[3].replaceAll(/Under attack by /gm, ""),
                            defenderstrength: "",
                            attackerstrength: "",
                            strength: StrengthEstimate(),
                            type: "continent-reinforcements",
                            starttime: descriptionMS(60),
                            starttimeISO: parse_ISO_date(descriptionMS(60)),
                            endtime: descriptionMS(120),
                            endtimeISO: parse_ISO_date(descriptionMS(120)),
                            timelength: "60",
                        })
                    break;
                }
            break;
            case 'goldstar':
                switch (currentTerritoryProperties.bountiful) {
                    case true:
                        fightList.push({
                            continent: continent,
                            capital: "",
                            territory: currentTerritoryData[2],
                            bounty: currentTerritoryData[4],
                            resources: currentTerritoryData[5],
                            type: "capital-bountiful",
                            begin: descriptionMS(0),
                            beginISO: parse_ISO_date(descriptionMS(0)),
                            expire: parse_cryptite_date(currentTerritoryData[6].replaceAll(/Ends at /gm, ""), true),
                            expireISO: parse_ISO_date(parse_cryptite_date(currentTerritoryData[6].replaceAll(/Ends at /gm, ""), true))
                        })
                    break;
                }
            break;
            case 'house':
                switch(currentTerritoryData.length) {
                    case 4:
                        townList.push({
                            continent: continent,
                            town: currentTerritoryData[0],
                            alliance: "Unaligned",
                            strength: currentTerritoryData[1].replaceAll(/\.0 strength/gm, "").replaceAll(/ strength/gm, ""),
                            members: currentTerritoryData[2].replaceAll(/ members/gm, ""),
                            territories: currentTerritoryData[3].replaceAll(/ territories/gm, ""),
                            capital: false,
                        })
                    break;
                    case 5:
                        townList.push({
                            continent: continent,
                            town: currentTerritoryData[0],
                            alliance: currentTerritoryData[1],
                            strength: currentTerritoryData[2].replaceAll(/\.0 strength/gm, "").replaceAll(/ strength/gm, ""),
                            members: currentTerritoryData[3].replaceAll(/ members/gm, ""),
                            territories: currentTerritoryData[4].replaceAll(/ territories/gm, ""),
                            capital: false,
                        })
                    break;
                }
            break;
            case 'shield':          
                switch(currentTerritoryData.length) {
                    case 5:
                        townList.push({
                            continent: continent,
                            town: currentTerritoryData[0],
                            alliance: "Unaligned",
                            strength: currentTerritoryData[2].replaceAll(/\.0 strength/gm, "").replaceAll(/ strength/gm, ""),
                            members: currentTerritoryData[3].replaceAll(/ members/gm, ""),
                            territories: currentTerritoryData[4].replaceAll(/ territories/gm, ""),
                            capital: true,
                        })
                        capital_list.push({
                            continent: continent,
                            town: currentTerritoryData[0],
                            alliance: "Unaligned",
                            strength: currentTerritoryData[2].replaceAll(/\.0 strength/gm, "").replaceAll(/ strength/gm, ""),
                            members: currentTerritoryData[3].replaceAll(/ members/gm, ""),
                            territories: currentTerritoryData[4].replaceAll(/ territories/gm, ""),
                            capital: true,
                        })
                    break;
                    case 6:
                        townList.push({
                            continent: continent,
                            town: currentTerritoryData[0],
                            alliance: currentTerritoryData[2],
                            strength: currentTerritoryData[3].replaceAll(/\.0 strength/gm, "").replaceAll(/ strength/gm, ""),
                            members: currentTerritoryData[4].replaceAll(/ members/gm, ""),
                            territories: currentTerritoryData[5].replaceAll(/ territories/gm, ""),
                            capital: true,
                        })
                        capital_list.push({
                            continent: continent,
                            town: currentTerritoryData[0],
                            alliance: currentTerritoryData[2],
                            strength: currentTerritoryData[3].replaceAll(/\.0 strength/gm, "").replaceAll(/ strength/gm, ""),
                            members: currentTerritoryData[4].replaceAll(/ members/gm, ""),
                            territories: currentTerritoryData[5].replaceAll(/ territories/gm, ""),
                            capital: true,
                        })
                    break;
                }
                console.log(capital_list)

            break;
            case 'caution':
                yellowInfested.push(HTMLRemoval(territorySet[territoryNames[x]].label)[2] ?? "Unknown Territory")
            break;
            case 'warning':
                redInfested.push(HTMLRemoval(territorySet[territoryNames[x]].label)[2] ?? "Unknown Territory")
            break;
            case 'silvermedal':
                switch (currentTerritoryProperties.continentType) {
                    case 'Continent':
                        switch (currentTerritoryData.length + currentTerritoryData.lengthOffset) {
                            case 2:
                                ContinentTerritories.push({
                                    continent: continent,
                                    alliance: "None",
                                    town: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                    territory: currentTerritoryData[1],
                                })
                            break;
                            case 4:
                                ContinentTerritories.push({
                                    continent: continent,
                                    alliance: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                                    town: currentTerritoryData[1].replaceAll(/Owner: /gm, ""),
                                    territory: currentTerritoryData[2],
                                })
                            break;
                        }
                    break;
                    case 'Rivina':
                        RivinaTerritories.push({
                            continent: continent,
                            owner: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                            territory: currentTerritoryData[1],
                            cp: currentTerritoryData[2].replaceAll(/ CP/gm, ""),
                        })
                    break;
                    case 'Balak':
                        BalakTerritories.push({
                            continent: continent,
                            owner: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                            territory: currentTerritoryData[1],
                            buff: "None",
                        })
                    break;
                }
            break;
            case 'temple':
                BalakTerritories.push({
                    continent: continent,
                    owner: currentTerritoryData[0].replaceAll(/ Territory/gm, ""),
                    territory: currentTerritoryData[1],
                    buff: balakBuffParser(currentTerritoryData[2]),
                })
            break;
        }
    }
    var outputData = new Output(townList,fightList,yellowInfested,redInfested,ContinentTerritories,RivinaTerritories,BalakTerritories);

    /*if (continent != "Balak" && continent != "Rivina") {
        const infested_tiles = infested_tiles_collection.document(continent);
        infested_tiles.red_infested = redInfested;
        infested_tiles.yellow_infested = yellowInfested;

        const capitals = capitals_collection.document(continent)
        capitals.capitals = capital_list
    }

    console.log("THIS IS A BIG TEST")*/
    
    return outputData;
}