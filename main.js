import fetch from "node-fetch";
import { stringify } from "querystring";
import { json } from "stream/consumers";
import { FetchData }  from "./data.js";
import { FightSend, sleep7573_lmao, sync_events, InfestedSend, TownSend, reset_event_map } from "./webhook.js";
import { BasicTerritoryParser } from "./parser.js"
import { biomeParser } from "./biomeparser.js";
import { status } from "./status.js";
import lib_fs from "fs";
import { townParser } from "./townparser.js";
import { post_status } from "./rtc.js"
//import { datastore } from "./data/base.js";

const environment = JSON.parse(lib_fs.readFileSync("dynamic_config.json")).environment
const ping_time = parseInt(JSON.parse(lib_fs.readFileSync("dynamic_config.json")).ping_time)
const data_endpoints = JSON.parse(lib_fs.readFileSync("static_config.json")).endpoints
const rules = JSON.parse(lib_fs.readFileSync("dynamic_config.json")).rules

export function calculate_fight_hash(fight_entry) {;
	var hashObj = {
		continent: fight_entry.continent,
		territory: fight_entry.territory,
	}
	const hash = JSON.stringify(hashObj);
	return hash;
}

async function main7573() {
	reset_event_map();
	
	var statusMessage = []

	let seen_fights = [];
	try {
		const dat = JSON.parse(lib_fs.readFileSync("seen_fights.json"));
		if (!Array.isArray(dat)) throw new Error("seen_fights.json is not an array");
		seen_fights = dat;
	} catch (e) {
		console.warn("Failed to load seen_fights.json");
		console.warn(e);
	}

	var BalakData = await FetchData(data_endpoints.Balak, 'Balak');
	var AscalonData = await FetchData(data_endpoints.Ascalon, 'Ascalon');
	var KalrosData = await FetchData(data_endpoints.Kalros, 'Kalros');
	var GaramaData = await FetchData(data_endpoints.Garama, 'Garama');
	var RivinaData = await FetchData(data_endpoints.Rivina, 'Rivina');//https://map.lokamc.com/tiles/_markers_/marker_lilboi.json

	post_status(AscalonData.fights.length + KalrosData.fights.length + GaramaData.fights.length + RivinaData.length + BalakData.length)

	let fights = [];
	function tidy_data(data, hash_func = calculate_fight_hash) {
		return data.filter(entry => {
			const hash = hash_func(entry);
			fights.push(hash);
			if (seen_fights.indexOf(hash) > -1) {
				console.log("Skipping Ping: " + hash);
				return false;
			} else {
				return true;
			}
		});
	}

	if (rules.fight_pings == true) {
		await FightSend(tidy_data(GaramaData.fights))
		await FightSend(tidy_data(KalrosData.fights))
		await FightSend(tidy_data(RivinaData.fights))
		await FightSend(tidy_data(AscalonData.fights))
		await FightSend(tidy_data(BalakData.fights))
	}

	const infested_hash = JSON.stringify([KalrosData.infested, AscalonData.infested, GaramaData.infested]);
	let prev_infested_hash = "";
	try { prev_infested_hash = lib_fs.readFileSync("infested_hash.txt").toString(); }
	catch {}
	if (KalrosData.infested[0].length > 0 && GaramaData.infested[0].length > 0 && AscalonData.infested[0].length > 0) {
		if (infested_hash !== prev_infested_hash) {
			if (rules.infested_pings == true) {
				await InfestedSend(KalrosData, AscalonData, GaramaData)
			}
			lib_fs.writeFileSync("infested_hash.txt", infested_hash);
		}
	}

	/*function calculate_town_hash(town) {
		var town = {
			territory: town.territory,
			continent: town.continent,
		}
		return JSON.stringify(town);
	}

	let prev_town_dictionary = {};
	try { 
		prev_town_dictionary = JSON.parse(lib_fs.readFileSync("town_info.json").toString()).data; 
	}
	catch {}

	var current_town_dictionary = {};
	const all_towns = KalrosData.towns.concat(AscalonData.towns).concat(GaramaData.towns);
	for (const town of all_towns) current_town_dictionary[calculate_town_hash(town)] = town;
	//DO STUFF HERE

	var write_time = Date.now()

	if (prev_town_dictionary == {} || prev_town_dictionary == undefined || prev_town_dictionary == null) {
		console.log("EEEEE")
	} else if (prev_town_dictionary !== {}) {
		var memberPing;
		if (Date.now() - prev_town_dictionary.write_time <= 86400000) {
			write_time = Date.now()
			memberPing = true;
		} else if (Date.now() - prev_town_dictionary.write_time >= 86400000) {
			write_time = prev_town_dictionary.write_time;
			memberPing = false;
		}
		var townPingData = townParser(current_town_dictionary, prev_town_dictionary, memberPing);
		current_town_dictionary = townPingData.current_town_dictionary;
		prev_town_dictionary = townPingData.prev_town_dictionary;
		if (rules.town_pings == true) {
			TownSend(townPingData.pings)
		}
	}

	const new_town_dictionary = { 
		write_time: write_time,
		data: { ...current_town_dictionary, ...prev_town_dictionary },
	};
	lib_fs.writeFileSync("town_info.json", JSON.stringify(new_town_dictionary));*/

	statusMessage = statusMessage.concat(await sync_events(fights));

	lib_fs.writeFileSync("seen_fights.json", JSON.stringify(fights));

	status(statusMessage, environment);
	var CurrentRunDate = new Date()

	/*await datastore.write_all();
	console.log("Datastore Written")
	console.log("Successful Run at " + CurrentRunDate)*/
}

async function do7573() {
	while (true) {
		try {
			await main7573();
		} catch(e) {
			console.error("IT DIED SOMEHOW OH NO -7573");
			console.error(e);
		}
		await sleep7573_lmao(ping_time * 60);
	}
}
do7573();