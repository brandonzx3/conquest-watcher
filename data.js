import fetch from "node-fetch";
import TerritoryParser from "./parser.js"

export async function FetchData(url, continent) {
    return await (fetch(url))
        .then(response => response.json())
        .then(data => TerritoryParser(data, continent));
}