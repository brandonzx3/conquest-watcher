import lib_fs from "fs";
import fetch from "node-fetch";
export function status (input_message, environment) {
    var statusDate = new Date();
    statusDate.toISOString();

    var ping = "";
    var additive_message = "";

    for (const i in input_message) {
        additive_message += "\n" + input_message[i]
    }
    
    console.log(input_message)

    if (input_message.length > 0) {
        ping = "**Additional Information Present**"  
    }

    var statusEmbed = {
        title: "Conquest Watcher Status",
        type: "rich",
        description: "**Status** :white_check_mark: Conquest Watcher Operational " + additive_message + " from **" + environment + "**",
        timestamp: statusDate,
        color: 0x2ECC71,
    };

    var statusPayload = {
        content: ping,
        embeds: [statusEmbed]
    };

    function webhookSend (content) {
        //console.log(JSON.stringify(content));
        fetch("https://discord.com/api/webhooks/917644376467984415/DdSQ39HGtWzGwEGeHIHIpT4TxeR5alIIk55JA73lv4t7IYW0ryyzNWrunPQmobeE92KS", {
            method: "POST",
            body: JSON.stringify(content),
            headers: {
                "Content-Type": "application/json",
                "Host": "discord.com",
            }
        }).then(response => {
            return response.text()
        }).then(data => console.log(data));
    };

    webhookSend(statusPayload);
}