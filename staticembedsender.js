import fetch from "node-fetch";
import * as lib_fetch from "node-fetch";
import { stringify } from "querystring";
import { json } from "stream/consumers";
import { timeStamp } from "console";

console.log(Object.keys(lib_fetch));

function webhookSend (content) {
    console.log(JSON.stringify(content));
    fetch('https://discord.com/api/webhooks/917632577844678666/tPhmep_dw1K0RjL9pZL8NGwxUPOxT9grVEaKiI27DAi6ZduoqOREMNvNpUoCWTlMurk1', {
        method: "POST",
        body: JSON.stringify(content),
        headers: {
            "Content-Type": "application/json",
            "Host": "discord.com",
        }
    }).then(response => {
        return response.text()
    }).then(data => console.log(data));
};

var date = new Date().toISOString();

var testEmbed = {
    title: "**:scroll: Welcome to Loka Conquest Watcher**",
    type: "rich",
    description: "🔎 **Loka Conquest Watcher** is a small side project that myself (FoxyBearGames), brandonzx3, and Christian7573 have been working on.\n\nIn essence, the goal of the project is to provide useful information about upcoming fights on **Kalros, Ascalon, and Garama,** as well as the **Conquest Isles.** Additionally, you also get a **ping** for every fight on each continent that you **choose to opt into** so you're able to stay in the loop.\n\nWe're able to provide data such as the **defender's strength, the territory the fight is on, and the buff or conquest point amount of the territory in the case of Balak and Rivina.**\n\nTo see conquest channels, you'll want to **react with the respective emoji pertaining to each continent.** Additionally, if you wish to **follow conquest pings in your own Discord,** you can do so by simply clicking the **follow button** in each channel!",
    timestamp: date,
    color: 0xf09f26,
    footer: {
        text: "Conquest Watcher",
        icon_url: "https://images-ext-2.discordapp.net/external/o9GgDUurykOmY6v0VKEr-j29Ew5YbG_6ysYaCAf0Drg/%3Fwidth%3D659%26height%3D702/https/media.discordapp.net/attachments/915861677965189180/917635653435531324/ConquestWatcher.png",
    },
    author: {
        name:"FoxyBearGames",
        url:"https://forums.lokamc.com/members/foxybeargames.1824/",
        icon_url: "https://cdn.discordapp.com/avatars/350086578133008386/1d8e177cd83f21f1a006794e720a9f7a.png"
    }
};

var payload = {
    content: "",
    embeds: [testEmbed]
};

webhookSend(payload);