import fetch from "node-fetch";
import lib_fs from "fs";

const client_id = JSON.parse(lib_fs.readFileSync('static_config.json')).twitch.client_id
const client_secret = JSON.parse(lib_fs.readFileSync('static_config.json')).twitch.client_secret
const streamers = JSON.parse(lib_fs.readFileSync('static_config.json')).twitch.streamers
const keys = JSON.parse(lib_fs.readFileSync("static_config.json")).webhook_keys
const environment = JSON.parse(lib_fs.readFileSync("dynamic_config.json")).environment
const servers = JSON.parse(lib_fs.readFileSync("static_config.json")).servers

var current_server_id;
var current_server_invite;

switch (environment) {
    case "DEVELOPER ENVIRONMENT":
        current_server_id = servers.FoxBox.server_id
        current_server_invite = servers.FoxBox.server_invite
    break;
    case "RELEASE ENVIRONMENT":
        current_server_id = servers.Conquest_Watcher.server_id
        current_server_invite = servers.Conquest_Watcher.server_invite
    break;
}

const twitch_access_token = await fetch ("https://id.twitch.tv/oauth2/token", {
    method: "POST",
    headers: {
        "content-type":"application/x-www-form-urlencoded"
    },
    body: "client_id=" + client_id + "&client_secret=" + client_secret + "&grant_type=client_credentials"
}).then(res => res.json())

function name_formatter (input) {
    var output = input.replaceAll(/Battle for /gm,"").replaceAll(/Reinforced /gm, "")
    return output;
}

var raw_events = JSON.parse(lib_fs.readFileSync('fight_events.json'))
var fights = [];

try {for (const element in raw_events) {
    fights.push({
        territory: name_formatter(raw_events[element].name),
        event_id: raw_events[element].id,
        event_map_id: raw_events[element],
    })
}} catch {}

let streamer_users = [];

for (let current_streamer of streamers) {
    var streamer_user = await fetch ("https://api.twitch.tv/helix/users?login=" + current_streamer, {
    method: "GET",
    headers: {
        "Authorization": "Bearer " + twitch_access_token.access_token,
        "Client-Id": client_id
    }
    }).then(res => res.json())
    streamer_users.push(streamer_user.data[0])
}

let streamer_channels_info = []

for (let i = 0; i < streamer_users.length; i++) {
    var get_channel_info = await fetch ("https://api.twitch.tv/helix/channels?broadcaster_id=" + streamer_users[i].id, {
    method: "GET",
    headers: {
        "Authorization": "Bearer " + twitch_access_token.access_token,
        "Client-Id": client_id
    }
    }).then(res => res.json())
    streamer_channels_info.push(get_channel_info.data[0])
}

function stream_send (input) {
    console.log(input)
    for (var stream of input) {
        var stream_title = stream.title.replaceAll(/Loka Minecraft - /gm, "").replaceAll(/ Defends /gm, "///").replaceAll(/ Against /gm, "///")
        stream_title = stream_title.split(/\/\/\//)
        var defender = stream_title[0]
        var attacker = stream_title[2]
        var territory = stream_title[1]
        for (const fight in fights) {
            if (fights[fight].territory == territory) {
                var contentMessage = "@everyone :tv: " + stream.broadcaster_name + " Is now Streaming the" + raw_events[fights[fight].event_map_id].name + " :tv:"

                var stream_link = "https://www.twitch.tv/" + stream.broadcaster_login

                var content = {
                    content: contentMessage + "\n" + stream_link + "\n" + `${current_server_invite}?event=${raw_events[fights[fight]].id}`
                };

                await fetch(keys.Streams + "?wait=true", {
                    method: "POST",
                    body: JSON.stringify(content),
                    headers: {
                        "Content-Type": "application/json",
                        "Host": "discord.com",
                    }
                }).then(async response => {
                    const raw_response = await response.text();
                    console.log(raw_response);
                    return JSON.parse(raw_response);
                }).then(data => {
                    console.log(data);
                        return fetch(`https://discord.com/api/v9/channels/${data.channel_id}/messages/${data.id}/crosspost`, {
                            method: "POST",
                            headers: {
                                "Authorization": "Bot OTE2OTI0MjE5NDA3ODc2MTI3.YaxOOQ.1r2Usl18oNSunL86MhsBkvTmtzg",
                            }
                        })
                        .then(res => res.text()).then(text => console.log(text));
                    });
            }
        }
    }
}

function calculate_stream_hash(stream_entry) {;
	var hashObj = {
		stream_title: stream_entry.title,
		broadcaster: stream_entry.broadcaster_login,
	}
	const hash = JSON.stringify(hashObj);
	return hash;
}

let seen_streams = [];
try {
    const dat = JSON.parse(lib_fs.readFileSync("seen_streams.json"));
    if (!Array.isArray(dat)) throw new Error("seen_streams.json is not an array");
    seen_streams = dat;
} catch (e) {
    console.warn("Failed to load seen_streams.json");
    console.warn(e);
}

let streams = [];
function stream_hash(data, hash_func = calculate_stream_hash) {
    return data.filter(entry => {
        const hash = hash_func(entry);
        streams.push(hash);
        if (seen_streams.indexOf(hash) > -1) {
            console.log("Skipping Stream: " + hash);
            return false;
        } else {
            return true;
        }
    });
}

await stream_send(stream_hash(streamer_channels_info))

lib_fs.writeFileSync("seen_streams.json", JSON.stringify(streams));

async function minecraft_server_status (server_address) {
    var data = await (fetch("https://api.mcsrvstat.us/2/" + server_address)).then(response => response.json())
    console.log(data);
}

console.log(minecraft_server_status('play.lokamc.com'))