import { Datastore, DocumentStruct } from "./datastore.js"; 

const datastore = new Datastore("./test_datastore");
await datastore.open();

///Literally holds a number called `counter`
class Ledger extends DocumentStruct {
	document_init() {
		console.log(`Document ${this.id} loaded into memory`);
	}

	get counter() { return this.get("counter", () => 0); }
	set counter(val) { this.set("counter", val); }
}

await datastore.register_collection("ledgers", Ledger);
const ledgers = datastore.collections.ledgers;

const lol1 = await ledgers.document("lol1");
console.log("\nlol1:");
console.log(lol1.counter);
lol1.counter += 1;
console.log(lol1.counter);

const reset_always = await ledgers.document("reset_always");

console.log("\nreset_always:");
console.log(reset_always.counter);
reset_always.counter = 0;
console.log(reset_always.counter);

const random_doc_id = `random${Math.round(Math.random() * 1000)}`;
const random_doc = await ledgers.document(random_doc_id);
console.log("\n" + random_doc_id);
console.log(random_doc.counter);
random_doc.counter += 1;
console.log(random_doc.counter);

console.log("\nFor each:");
await ledgers.for_each(doc => {
	console.log(`${doc.id}: ${doc.counter} => ${doc.counter + 1}`);	
	doc.counter++;
}, true);

await datastore.write_all();
