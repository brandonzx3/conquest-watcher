//Runtime type checking building blocks

/** Builds a verifier that ensures data is of the specified type
 * @param type - the type to ensure */
export function verify_type(type) { return (obj) => typeof obj === type ? null : `Not of type ${type}`; }
/** Builds a verifier that ensures data is of the specified structure.
 * @param struct - an object, where the value of each key is a verifier for that key */
export function verify_struct(struct) {
	return (obj) => typeof obj === "object" || typeof obj === "function" ?
		Array.from(Object.keys(struct))
		.map(item => { const e = struct[item](obj[item]); return e == null ? null : `Field "${item}": < ${e} >`; })
		.reduce((a, b) => a == null ? b : a + (b == null ? "" : `,${b}`))
	: "Not of type object or function";
}
/** Builds a verifier that ensures data is an array where each elements meets criteria
 * @param check - a verifier to check each element of an array */
export function verify_collection(check) {
	return (obj) => Array.isArray(obj) ?
		obj.map((item, i) => { const e = check(item); return e == null ? null : `Element ${i}: < ${e} >`; })
		.reduce((a, b) => a == null ? b : a + (b == null ? "" : `,${b}`))
	: "Not of type array";
}
/** Builds a verifier that ensures a tuple matches the specified format
 * @param template - a tuple of verifiers to check against,
 * @param strict_length - whether the length must match exactally, defaults to false */
export function verify_tuple(template, strict_length = false) {
	return (obj) => Array.isArray(obj) ?
		!strict_length || obj.length === template.length ?
			template.map((check, i) => { const e = check(obj[i]); return e == null ? null : `Element ${i}: < ${e} >`; })
			.reduce((a, b) => a == null ? b : a + (b == null ? "" : `,${b}`))
		: `Not of length ${template.length}`
	: "Not of type array";
}
/** Builds a verifier that verified data only if it is present (not null)
 * @param check - the verifier to use if the object exists */
export function verify_optional(check) { return (obj) => obj == null ? null : check(obj) }
/** Builds a verifier that combines two other verifiers
 * @param a - verifier 1
 * @param b - verifier 2 */
export function verify_both(a, b) { return (obj) => { const ar = a(obj); return ar == null ? b(obj) : ar; } }
/** Builds a verifier that runs a verifier on an object's prototype
 * @param check - the verifier */
export function verify_prototype(check) { return (obj) => {
	var r = null;
	if (typeof obj === "object") r = check(Object.getPrototype(obj));
	else if (typeof obj === "function") r = check(obj.prototype);
	else return "Not of type object or function";
	if (r !== null) r = "Prototype: " + r;
	return r;
}; };
/** Builds a verifier that ensures an object is an instance of a specific class
 * @param classs - the class to check for */
export function verify_instance_of(classs) { return (obj) => obj instanceof classs ? null : `Not an instance of ${classs.name ?? classs}`; }

/**
 * Builds a function to verify the structure of data at runtime
 * @param verify_function - a function built out of various "verify_" building blocks, which generate functions that return null on success or a string on error
 */
export function verify(verify_function) {
	return function(thing_to_verify) {
		const result = verify_function(thing_to_verify);
		if (result != null) throw new Error(result);
	}
}

/** Convienience wrapper for all of them */
export const verifiers = {
	type: verify_type,
	struct: verify_struct,
	collection: verify_collection,
	tuple: verify_tuple,
	optional: verify_optional,
	both: verify_both,
	prototype: verify_prototype,
	instance_of: verify_instance_of,
	verify: verify,
};


/** Recursivly runs a function on each value (including itself) in a JSON object
 * @param val - the value to recurse through
 * @param func - the function to run for each value
 * @param include_prototype - whether to recurse through function prototypes, defaults to false */
export function recurse_each_value(val, func, include_prototype = false) {
	const seen_objects = new Set();
	function recurse(val) {
		if (typeof val === "object" || typeof val === "function") {
			if (seen_objects.has(val)) return;
			seen_objects.add(val);
		}
		func(val);
		if (include_prototype) recurse(Object.getPrototypeOf(val));
		for (const key in val) func(val[key]);
	}
	recurse(val);
}

/** Deep clones a value by converting it to JSON and back
  * @template T
  * @param {T} val
  * @returns {T} */
export function wack_clone(val) {
	return JSON.parse(JSON.stringify(val));
}

/** A wrapper around promises for inspecting their state
  * @template V */
export class InspectablePromise {
	/** Whether the promise finished, by either resolving or rejecting */
	finished = false;
	/** Whether the promise resolved */
	resolved = false;
	/** Whether the promise rejected */
	rejected = false;
	/** The value the promise resolved with, if it's resolved
	  * @type {V} */
	resolved_value = null;
	/** The value the promise rejected with, if it's rejected */
	rejected_value = null;

	/** A wrapper around promises for inspecting their state
	  * @param {Promise<V>} promise - the promise to wrap */
	constructor(promise) {
		this.promise = promise;
		promise.then(
			(v) => { this.finished = true; this.resolved = true; this.resolved_value = v; },
			(e) => { this.finished = true; this.rejected = true; this.rejected_value = e; },
		);
	}
}

/** Convienience wrapper to validate function arguments
 * @param {any[]} args - pass `arguments` here
 * @param {number} count - the number of required arguments
 * @param {function} verifier - an optional function to verify arguments */
export function argt(args, count, verifier) {
	if (args.length < count) throw new Error(`Invalid argument count, missing ${count - args.length} arguments`);
	if (typeof verifier === "function") {
		const result = verifier(Array.from(args));
		if (result != null) throw new Error("Invalid arguments: " + result);
	}
}
