import lib_fs from "fs";
import { verifiers as v, InspectablePromise, argt } from "./typeutils.js";

const { F_OK, R_OK, W_OK, X_OK } = lib_fs.constants;

/** Ensure a directory exists and is usable
  * @param {string} directory - the path of the directory to ensure */
async function ensure_directory(directory) {
	argt(arguments, 1, v.tuple([v.type("string")]));
	try {
		//Fabricate it
		try { await lib_fs.promises.mkdir(directory, { recursive: true }); }
		catch (e) {
			//Error -17 is "already exists" on linux atleast
			if (e.errno !== -17) throw e;
		}
		//Ensure it exists
		const stats = await lib_fs.promises.stat(directory);
		if (!stats.isDirectory) throw new Error("Not a directory");
		//Ensure we can use it
		await lib_fs.promises.access(directory, F_OK | R_OK | W_OK | X_OK);
	} catch (e) {
		//Rewrap error for niceness
		throw new Error("Failed to ensure directory `" + directory + "`", { cause: e });
	}
}

/** A structure to handle read/writing data from disk */
export class Datastore {
	/** A structure to handle read/writing data from disk
	  * @param {string} location - the location on disk for the datastore */
	constructor(location) {
		argt(arguments, 1, v.tuple([v.type("string")]));
		this._location = location;
	}

	/** @private */
	_location = null;
	/** The root folder of the datastore
	  * @type { string } */
	get location() {
		if (typeof this._location !== "string") throw new Error("No datastore location configured");
		return this._location;
	}
	/** The folder where document collections will be stored
	  * @type { string } */
	get store_location() { return this.location + "/store"; }
	/** The folder where tag lists will be stored
	  * @type { string } */
	get tag_location() { return this.location + "/tag"; }

	/** @private */
	_opened = false;
	/** Whether the datastore has been opened yet */
	get opened() { return this._opened; }
	/** Open the datastore for use */
	async open() {
		try {
			//Ensure all of the things exist
			await ensure_directory(this.location);
			await ensure_directory(this.store_location);
			await ensure_directory(this.tag_location);
			this._opened = true;
		} catch (cause) {
			throw new Error("Failed to open datastore", { cause });
		}
	}

	//lmao what type do I give this abomination
	/** All of the collections in the datastore 
	  * @type {Record<string, Collection<DocumentStruct>>} */
	collections = {}

	/** Registers a document struct as a collection
	 * @param {string} name - the name of the collection,
	 * @param {typeof DocumentStruct} struct - the DocumentStruct class for this collection's documents */
	async register_collection(name, struct) {
		argt(arguments, 2);
		try {
			validate_storable(name);
			const dir = this.store_location + "/" + name;
			await ensure_directory(dir);
			verify_DocumentStruct(struct);
			this.collections[name] = new Collection(name, dir, struct);
		} catch (cause) {
			throw new Error(`Failed to register collection ${name}`, { cause });
		}
	}

	/** Writes all documents in all collections to disk */
	async write_all() {
		for (const collection in this.collections) await this.collections[collection].write_all();
	}
}

/** Validates the name of a storable, aka a string that will become part of a file name
 * @param {string} storable - the string to validate */
function validate_storable(storable) {
	const regex = /^[a-zA-Z0-9_\-]+$/;
	if (typeof storable !== "string" || !regex.test(storable)) throw new Error("`" + storable + "` is not a valid storable");
}

/** a function to be run on each element of a collection
 * @callback ForEach
 * @template T
 * @param {T} item */

/** An association between data on disk and a class
 * @template {DocumentStruct} S */
export class Collection {
	constructor(name, location, struct) {
		argt(arguments, 3);
		validate_storable(name);
		this._name = name;
		this._struct = struct;
		this._location = location;
	}

	/** @private */
	_name = null;
	/** The name of this collection
	  * @type {string} */
	get name() { return this._name; }

	/** @private */
	_struct = null;
	/** The struct associated with these documents
	  * @type {typeof S} */
	get struct() { return this._struct; }

	/** @private */
	_location = null;
	/** The folder where documents are stored on disk.
	  * @type {string} */
	get location() { return this._location; }

	//This this is atroshious
	/** @private
	  * @type {Map<string, InspectablePromise<[DocumentStruct, boolean]>> */
	_in_memory_documents = new Map();

	/** Get (or create) a document from this collection
	 * @param {string} id - the id of the document
	 * @param {boolean} strong - internal caching behavior, defaults to true
	 * @returns {Promise<S>} the resulting document */
	async document(id, strong = true) {
		argt(arguments, 1, v.tuple([v.type("string"), v.optional(v.type("boolean"))]));

		//Check the loaded documents
		var promise = this._in_memory_documents.get(id);
		if (promise == null) {
			//The document is not in the cache, load it now
			promise = new InspectablePromise((async () => {
				const location = this.location + "/" + id + ".json";
				//Ensure access rights to the file
				try { await lib_fs.promises.access(location, F_OK | R_OK | W_OK); }
				catch (cause) {
					//Error code -2 is "doesn't exist" on linux atleast
					if (cause.errno !== -2) throw new Error(`Incorrect access rights to document "${id}" in container "${this.name}"`, { cause });
				}

				//Read in data if it already exists
				let data = {};
				try {
					const actual_data = JSON.parse(await lib_fs.promises.readFile(location));
					if (typeof actual_data === "object") data = actual_data;
				}
				catch (cause) {
					//Error code -2 is "doesn't exist" on linux atleast
					if (cause.errno !== -2) throw new Error(`Unable to load document "${id}" in container "${this.name}"`, { cause });
				}

				//Create, cache, and return
				const doc = new this.struct();
				doc._document_data = data;
				doc._document_id = id;
				doc._document_location = location;
				doc._document_collection = this;
				doc.document_init();

				return [doc, strong];
			})()
			.catch(cause => { 
				//If loading failed, remove it from the cache
				this._in_memory_documents.delete(id);
				throw new Error(`Failed to load document "${id}" from collection "${this.name}"`, {cause})
			}));
			this._in_memory_documents.set(id, promise);
		}

		const res = await promise.promise;
		if (strong) res[1] = true;
		return res[0];
	}

	/** Writes all documents to disk */
	async write_all() {
		for (const stuff of this._in_memory_documents.values()) {
			if (stuff.resolved) await stuff.resolved_value[0].document_write();
		}
	}

	/** Attempts to unload a document from memory. This does not write it to disk
	 * @param {string} id 
	 * @param {boolean} only_if_weak - only unload the document if it is not strongly loaded, defaults to false
	 * @returns {boolean} whether it actually was unloaded */
	unload(id, only_if_weak = false) {
		argt(arguments, 1, v.tuple([v.type("string"), v.optional(v.type("boolean"))]));
		const stuff = this._in_memory_documents.get(id);
		if (stuff == null) return false;
		if (!stuff.resolved) return false;
		if (only_if_weak && stuff.resolved_value[1]) return false;
		this._in_memory_documents.delete(id);
	}
	
	/** Writes all documents to disk, and then removes them from memory
	 * @param {boolean} only_if_weak - only unload documents not strongly loaded, defaults to false */
	async unload_all(only_if_weak = false) {
		argt(arguments, 0, v.tuple([v.optional(v.type("boolean"))]));
		await this.write_all();
		if (only_if_weak) {
			for (const key of this._in_memory_documents.keys()) {
				this.unload(key, only_if_weak);
			}
		} else {
			this._in_memory_documents = new Map();
		}
	}

	/** Returns a list of all document ids in this collection
	  * @returns {Promise<string[]>} - the document ids in this collection */
	async list() {
		return new Set(
			(await lib_fs.promises.readdir(this.location).catch(cause => { throw new Error(`Failed to list documents for collection "${this.name}"`, {cause}); }))
			.filter(doc => doc.length > 6 && doc.substring(doc.length - 5) === ".json")
			.map(doc => doc.substring(0, doc.length - 5))
			.concat(Array.from(this._in_memory_documents.keys()))
		);
	}

	/** Executes a function on every document in this database. If they're not already loaded into memory,
	 * it will load it tempoarily, and unload it afterward. Optionally writes everything back to the disk
	 * if you specify. Supports async operations.
	 * @param {ForEach<S>} func - the function to execute on each item
	 * @param {boolean} write_to_disk - whether to write to the disk after each execution
	 * @param {boolean} parallel - whether to execute async operations in parallel, defaults to false */
	async for_each(func, write_to_disk, parallel = false) {
		argt(arguments, 2, v.tuple([v.type("function"), v.type("boolean"), v.optional(v.type("boolean"))]));
		const entries = await this.list();
		const for_each = async (id) => {
			const doc = await this.document(id, false);
			await Promise.resolve(func(doc));
			if (write_to_disk) await doc.document_write();
			this.unload(id, true);
		};
		if (parallel) {
			const promises = entries.map(doc => for_each(doc));
			for (const promise of promises) await promise;
		} else {
			for (const doc of entries) await for_each(doc);
		}
	}
}

/** Creates an initial value for the property
 * @callback PropertyInitializer
 * @template V
 * @returns {V} - the initial value */

/** Data, encapsulated in a document. Not to be used directly, but rather extended by other classes,
 * and then registered with Datastore.Register */
export class DocumentStruct {
	/** @private */
	_document_collection = null;
	/** The collection this document belongs to
	  * @type {Collection} */
	get document_collection() {
		if (!(this._document_collection instanceof Collection)) throw new Error("Invalid document collection");
		return this._document_collection;
	}

	/** @private */
	_document_id = null;
	/** The ID of this document. Cannot change
	  * @type {string} */
	get id() {
		if (typeof this._document_id !== "string") throw new Error("Invalid document id");
		return this._document_id;
	}

	/** @private */
	_document_location = null;
	/** The location on disk where the document is stored
	  * @type {string} */
	get document_location() {
		if (typeof this._document_location !== "string") throw new Error("Invalid document location");
		return this._document_location;
	}

	/** @private */
	_document_data = {};

	/** Gets a property. If it has not been initialized, it calls the provided initializer function
	 * @template V
	 * @param {string} property - the property to get
	 * @param {PropertyInitializer<V>} initializer - the initializer if the property hasn't been initialized yet
	 * @returns {V} the value of the property */
	get(property, initializer) {
		argt(arguments, 2, v.tuple([v.type("string"), v.type("function")]));
		if (this._document_data[property] == null) this._document_data[property] = initializer();
		return this._document_data[property];
	}

	/** Sets a property.
	 * @template V
	 * @param {string} property - the property to set
	 * @param {V} avlue - the value to set to the property */
	set(property, value) {
		this._document_data[property] = value;
	}

	/** Writes the document to disk */
	async document_write() {
		try {
			await lib_fs.promises.writeFile(this.document_location, JSON.stringify(this._document_data));
		} catch (cause) {
			throw new Error(`Failed to write document "${this.id}" in collection "${this.document_collection.name}"`, {cause});
		}
	}
}

const verify_DocumentStruct = v.verify(v.both(
	v.type("function"),
	v.prototype(v.both(
		v.struct({ document_init: v.type("function") }),
		v.instance_of(DocumentStruct),
	)),
));
